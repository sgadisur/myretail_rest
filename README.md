# myretail-service #

This is a Spring/Maven/Java simple REST service for products application for myRetail corp.

**How to Run:**

1. clone this repository

2. make sure you have Java 1.8 and Maven 3.x

3. You can build the project and run the tests by running mvn clean package

**About this service:**

This is a simple service for accessing and updating the product information for myRetail. It uses Cassandra- No SQL DB as data store, fetches the product related information from the external APIs.

These are the following resources available:

GET: http:/localhost:8080/product/{id}
PUT: http:/localhost:8080/product/{id}

** Testing the application:**

Use Postman REST client, a chrome browser extension, to simply test/access the resources.