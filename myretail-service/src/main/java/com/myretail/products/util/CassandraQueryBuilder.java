package com.myretail.products.util;

import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;

public class CassandraQueryBuilder {

	private String serverIp = "127.0.0.1";
//	private String port = "9042";
	private String keySpace = "myretail";
	
	//query Cassandra DB for Products table for current pricing
	public double getProductPrice(int id) {
		String cqlStatement = "select current_price from products where productid = " + id;
		double currentPrice = 0;
		Cluster cluster = Cluster.builder().addContactPoints(serverIp).build();
		Session session = cluster.connect(keySpace);
		//ResultSet resultSet = session.execute(query);
		for (Row row : session.execute(cqlStatement)) {
			  currentPrice = row.getDouble(0);
			}
		return currentPrice;
	}

	public String updateProductPrice(int id, Double newPrice) {
		String cqlStatement = "update products set current_price = "+ newPrice + "where productid = " + id;
		String message = "Price for the Product "+ id + "is successfully updated!";
		Cluster cluster = Cluster.builder().addContactPoints(serverIp).build();
		Session session = cluster.connect(keySpace);
		session.execute(cqlStatement);
		return message;
	}

}
