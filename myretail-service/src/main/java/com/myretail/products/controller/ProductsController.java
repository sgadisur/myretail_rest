package com.myretail.products.controller;

import java.io.IOException;
import org.json.JSONException;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.myretail.products.bean.CurrentPrice;
import com.myretail.products.bean.MyretailServiceURIConstants;
import com.myretail.products.bean.Product;
import com.myretail.products.util.CassandraQueryBuilder;
import com.myretail.products.util.GetProductName;

@RestController
public class ProductsController {

	@RequestMapping(value=MyretailServiceURIConstants.GET_PRODUCTS,method=RequestMethod.GET,headers="Accept=application/json")
	public Product getProductDetails(@PathVariable("id") int id) throws JSONException, IOException{
		
		GetProductName getProductName = new GetProductName();
		Product productDetails = new Product();
		CurrentPrice cp = new CurrentPrice();
		CassandraQueryBuilder cqb = new CassandraQueryBuilder();
		
		String prdName = getProductName.getName(id);
		cp.setValue(cqb.getProductPrice(id));
		cp.setCurrencyCode("USD");
		
		productDetails.setId(id);
		productDetails.setProductName(prdName);
		productDetails.setCurrentPrice(cp);
		
		return productDetails;
	}
	
	@RequestMapping(value=MyretailServiceURIConstants.PUT_PRODUCTS,method=RequestMethod.PUT,headers="Accept=application/json")
	public @ResponseBody String updateProductPrice(@RequestBody Product product){
		String message = null;
		
		int id = product.getId();
		Double newPrice = product.getCurrentPrice().getValue();
		CassandraQueryBuilder cqb = new CassandraQueryBuilder();
		message = cqb.updateProductPrice(id,newPrice);
		return message;
	}
	
}
