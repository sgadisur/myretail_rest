package com.myretail.products.bean;

public class MyretailServiceURIConstants {
	
	public static final String GET_PRODUCTS = "/product/{id}";
	public static final String PUT_PRODUCTS = "/product/{id}";

}
