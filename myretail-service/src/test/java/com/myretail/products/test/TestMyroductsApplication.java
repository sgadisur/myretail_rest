package com.myretail.products.test;

import org.springframework.web.client.RestTemplate;

import com.myretail.products.bean.Product;

public class TestMyroductsApplication {
	
	public static final String SERVICE_URI = "http://localhost:8080/product";
	
	public static void main(String[] args) {
		testGetProduct();
		System.out.println("----");
	}

	private static void testGetProduct() {
		RestTemplate restTemp = new RestTemplate();
		Product product = restTemp.getForObject(SERVICE_URI+"/13860428", Product.class);
		System.out.println(product);
	}

}
